import KCircle from "./circle";
import KButton from "./button";
const components = [KCircle,KButton];

const install = function (Vue) {
  if (install.installed) return;
  install.installed = true;
  components.map((component) => Vue.component(component.name, component));
  // components.map(component=>Vue.use(component))
};
if (window !== "undefined" && window.Vue) {
  install(window.Vue);
}

export default {
  install,
  ...components,
};
