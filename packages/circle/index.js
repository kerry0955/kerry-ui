import KCircle from "./src/circle.vue";

KCircle.install = (Vue) => {
  Vue.component(KCircle.name, KCircle);
};

export default KCircle;
